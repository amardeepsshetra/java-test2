import java.util.Scanner;


public interface TwoDimensionalShapeInterface {
	public void calculateArea();
	public void printInfo();
}

