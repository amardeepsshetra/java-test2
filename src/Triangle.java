import java.util.Scanner;

public class Triangle extends Shape implements TwoDimensionalShapeInterface {
	double base;
	double height;

	public Triangle(String color, String shape, double base, double height) {
		super(color, shape);
		// TODO Auto-generated constructor stub
		this.base = base;
		this.height = height;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public void calculateArea() {
		// TODO Auto-generated method stub
		super.calculateArea();
		double area = (base* height)/2;

	}

	@Override
	public void printInfo() {
		// TODO Auto-generated method stub
		super.printInfo();

		System.out.println(	 "selected Shape is: "+ this.shape+ "color of the shape : " + this.color + "\n" + "base of the shape :" + this.base
				+ " \n" + "height of the shape :" + this.height);
	}
}
